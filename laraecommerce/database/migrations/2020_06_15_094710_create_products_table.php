<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('brand_id');
            $table->integer('label_id');
            $table->integer('categories_id');
            $table->string('title');
            $table->string('picture');
            $table->mediumText('short_description');
            $table->longText('description');
            $table->integer('total_sales');
            $table->string('product_type');
            $table->tinyInteger('is_new');
            $table->float('cost');
            $table->float('mrp');
            $table->boolean('special_price')->nullable();
            $table->softDeletes('soft_delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
