<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Profile::class, function (Faker $faker) {
    

    return [
        'bio' => $faker->paragraph(5),
        'web' => $faker->url,
        'facebook' => 'https://www.facebook.com/' . $faker->unique()->firstName,
        'twitter' => 'https://www.twitter.com/' . $faker->unique()->firstName,
        'github' => 'https://www.github.com/' . $faker->unique()->lastName,
    ];
});
